package id.nanda.library.services.book;

import id.nanda.library.payloads.request.BookRequest;
import id.nanda.library.payloads.response.ResponseData;

public interface BookService {
 // kerangka method CRUD
 // create book
 ResponseData createBookService(BookRequest request);
 
 // read books
 ResponseData getBooksService(Boolean status);

 ResponseData getBookByIdService(Long idBook);

 //update
 ResponseData updateBookByIdService(Long idBook,  BookRequest request);

 // delete
 ResponseData deleteBookService(Long idBook);
}
